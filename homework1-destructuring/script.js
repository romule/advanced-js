//task1
const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const newArr = new Set([...clients1, ...clients2]);
console.log(newArr);

//task2
const characters = [
  {
    Name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    Name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    Name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human",
  },
  {
    Name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire",
  },
  {
    Name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire",
  },
  {
    Name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire",
  },
];

const charactersShortInfo = characters.map(
  ({ gender, status, ...rest }) => rest
);

console.log(charactersShortInfo);

//task3
const user1 = {
  name: "John",
  years: 30,
  // isAdmin: true,
};

const { name: name, years: age, isAdmin: isAdmin = false } = user1;

document.body.insertAdjacentHTML(
  "beforeend",
  ` <p>The "user1" object keys, are:</p> 
    <ul style="list-style: none;"> 
      <li>Name: ${name}</li>
      <li>Age: ${age}</li> 
      <li>isAdmin: ${isAdmin}</li>
    </ul>`
);

//task 4

const satoshi2020 = {
  name: "Nick",
  surname: "Sabo",
  age: 51,
  country: "Japan",
  birth: "1979-08-21",
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: "Dorian",
  surname: "Nakamoto",
  age: 44,
  hidden: true,
  country: "USA",
  wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
  browser: "Chrome",
};

const satoshi2018 = {
  name: "Satoshi",
  surname: "Nakamoto",
  technology: "Bitcoin",
  country: "Japan",
  browser: "Tor",
  birth: "1975-04-05",
};

const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };
console.log(fullProfile);

//task 5

const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};

const newBooksArray = [...books, bookToAdd];

//task 6

const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

const addKey = { ...employee, age: 47, salary: 1000000 };
console.log(addKey);

//task7
const array = ["value", () => "showValue"];

// Допишите ваш код здесь
[value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue()); // должно быть выведено 'showValue'

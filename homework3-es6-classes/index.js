document.body.innerHTML = "<h1>Open console</h1>";

class Employee {
  //call get-methods for class
  constructor(name, age, salary) {
    this.Name = name;
    this.Age = age;
    this.Salary = salary;
  }

  //creating variables and assign value
  set Name(value) {
    this.name = value;
  }
  set Age(value) {
    this.age = Number(value);
  }
  set Salary(value) {
    this.salary = Number(value);
  }

  //return value of variables
  get printName() {
    return this.name;
  }
  get printAge() {
    return this.age;
  }
  get printSalary() {
    return this.salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.Lang = lang;
  }

  set Lang(value) {
    this.lang = ["EN", "PL", "UA", "RU"];
  }

  get printSalary() {
    return this.salary * 3;
  }

  get printLeng() {
    return this.lang;
  }
}

const emp2 = new Programmer("Elijah", 28, 5000);
const emp3 = new Programmer("VLAD", 25, 6000);
const emp4 = new Programmer("Tania", 23, 10000);

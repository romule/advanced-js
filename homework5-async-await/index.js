async function ipSearch(url, btn) {
  let response = await fetch(url);
  let data = await response.json();

  data = JSON.stringify(data);
  data = data.replace(/[^\d.]/g, "");

  getIp(data).then((ip) => {
    btn.insertAdjacentHTML(
      "afterend",
      `<p>Continent: ${ip.continent} <br> 
      Country: ${ip.country} <br> 
      Region: ${ip.region} <br> 
      City: ${ip.city}<br> 
      District: ${ip.district}<br> 
      Zip-code: ${ip.zip}</p> `
    );
  });
}

async function getIp(ipStr) {
  let ip = await fetch(
    `http://ip-api.com/json/${ipStr}?fields=status,message,continent,country,region,city,district,zip`
  );
  let ipData = await ip.json();

  return ipData;
}

document.querySelector(".btn").addEventListener("click", () => {
  ipSearch(
    "https://api.ipify.org/?format=json",
    document.querySelector(".btn")
  );
});

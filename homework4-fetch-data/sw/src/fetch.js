let fetchUrl = (url) => {
  return fetch(url)
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      return data;
    });
};

let fetchAll = (mas) => {
  let trueMas = mas.map((e) => {
    return fetch(e);
  });

  return Promise.all(trueMas);
};

export { fetchUrl };
export { fetchAll };

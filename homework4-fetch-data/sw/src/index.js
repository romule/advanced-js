import { fetchUrl, fetchAll } from "./fetch";
import { toScreen } from "./toScreen";

let data = fetchUrl("https://swapi.dev/api/films/");

data.then((items) => {
  items.results.forEach((element) => {
    console.log(element);
    toScreen(
      `<div class="card episode_${element.episode_id}">
            <h4 class="card__caption">TITLE: ${element.title}</h4>
            <h4 class="card__caption">FILM #${element.episode_id}</h4>
            <h4 class="card__caption">TEXT-INTRO: "${element.opening_crawl}"</h4>
            <h4 class="card__caption">CHARACTERS:</h4>

        </div>`,
      ".container"
    );
  });

  items.results.forEach((element) => {
    let char = fetchAll(element.characters);
    char
      .then((e) => {
        return e;
      })
      .then((data) => {
        let x = data.map((item) => {
          return item.json();
        });
        let y = x.map((e) => {
          return e;
        });

        Promise.all(y).then((data) => {
          let list = data
            .map((char) => {
              return `${char.name}, `;
            })
            .join("");
          let listUl = `<p>${list}</p>`;

          toScreen(listUl, `.episode_${element.episode_id}`);
        });
      });
  });
});

let toScreen = (data, block) => {
  let target = document.querySelector(`${block}`);
  target.insertAdjacentHTML("beforeend", data);
};

export { toScreen };
